import 'package:equatable/equatable.dart';

class Post extends Equatable {
  const Post({
    required this.id,
    required this.title,
    required this.body,
    required this.image,
    required this.date

  });

  final int id;
  final String title;
  final String body;
  final String image;
  final String date;

  @override
  List<Object> get props => [id, title,
    body,
    image,
    date
  ];
}