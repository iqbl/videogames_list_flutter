import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:videogames_list_flutter/posts/posts.dart';
import 'package:http/http.dart' as http;
import 'package:stream_transform/stream_transform.dart';

part 'post_event.dart';
part 'post_state.dart';

const _postLimit = 20;
const throttleDuration = Duration(milliseconds: 100);

EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class PostBloc extends Bloc<PostEvent, PostState> {
  PostBloc({required this.httpClient}) : super(const PostState()) {
    on<PostFetched>(
      _onPostFetched,
      transformer: throttleDroppable(throttleDuration),
    );
  }

  final http.Client httpClient;

  Future<void> _onPostFetched(
      PostFetched event,
      Emitter<PostState> emit,
      ) async {
    if (state.hasReachedMax) return;
    try {
      if (state.status == PostStatus.initial) {
        final posts = await _fetchPosts();
        // log("posts= "+posts.toString());
        return emit(state.copyWith(
          status: PostStatus.success,
          posts: posts,
          hasReachedMax: false,
        ));
      }
      final posts = await _fetchPosts(state.posts.length);
      posts.isEmpty
          ? emit(state.copyWith(hasReachedMax: true))
          : emit(
        state.copyWith(
          status: PostStatus.success,
          posts: List.of(state.posts)..addAll(posts),
          hasReachedMax: false,
        ),
      );
    } catch (_) {
      emit(state.copyWith(status: PostStatus.failure));
    }
  }

  Future<List<Post>> _fetchPosts([int startIndex = 1]) async {
    // final response = await httpClient.get(
    //   Uri.https(
    //     'jsonplaceholder.typicode.com',
    //     '/posts',
    //     <String, String>{'_start': '$startIndex', '_limit': '$_postLimit'},
    //   ),
    // );

    final response = await http.get(
        Uri.parse('https://api.themoviedb.org/3/search/movie?api_key=6753d9119b9627493ae129f3c3c99151&query=superman&page=$startIndex'),
        // Uri.parse('https://jsonplaceholder.typicode.com/posts?_start=$startIndex&_postLimit=$_postLimit'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
    );

    if (response.statusCode == 200) {
      // final body = json.decode(response.body) as List;
      final body = json.decode(response.body)['results'] as List;

      log('lognya= '+body.toString());
      // log('lognya= '+ body.map((dynamic json) {
      //   return Post(
      //     id: json['id'] as int,
      //     title: json['title'] as String,
      //     body: json['body'] as String,
      //   );
      // }).toList().toString());
      return body.map(( json) {
        // log('lognya= '+json.toString());
        return Post(
          id: json['id'],
          title: json['title'],
          body: json['overview'],
          image: getPoster(json.toString()),
          date: getDate(json.toString()) ,
        );
      }).toList();


    }
    else log('lognya= '+response.statusCode.toString());
    throw Exception('error fetching posts');
  }

  getPoster(String body){
    // const str = body.toString();
    const start = "poster_path: ";
    const end = ", release_date:";

    final startIndex = body.indexOf(start);
    final endIndex = body.indexOf(end, startIndex + start.length);

    return body.substring(startIndex + start.length, endIndex);
    // log("Reults "+body.substring(startIndex + start.length, endIndex)); // brown fox jumps
  }

  getDate(String body){
    const start = " release_date: ";
    const end = ", title:";

    final startIndex = body.indexOf(start);
    final endIndex = body.indexOf(end, startIndex + start.length);

    return body.substring(startIndex + start.length, endIndex);

  }
}