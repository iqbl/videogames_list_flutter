import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:videogames_list_flutter/posts/posts.dart';

class PostListItem extends StatelessWidget {
  const PostListItem({Key? key, required this.post}) : super(key: key);

  final Post post;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return

      // Container(
      //   height: 500,
      //   child: Card(
      //     child: Image.network(
      //       'https://placeimg.com/640/480/any', fit: BoxFit.fill,),
      //     shape: RoundedRectangleBorder(
      //       borderRadius: BorderRadius.circular(10.0),
      //     ),
      //     elevation: 5,
      //     margin: EdgeInsets.all(10),
      //   ),
      // );


    Container(
      width: 200,
      child: Card(

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 10,
            ),
            Image.network(
              "https://image.tmdb.org/t/p/w92"+post.image,
              fit: BoxFit.fill,
            ),
            Container(
              height: 10,
            ),
            Align(
                // alignment: Alignment.bottomLeft,
                alignment: Alignment.bottomCenter,
                child:Container(
                  // transform: Matrix4.translationValues(0.0, -10.0, 0.0),
                  child:  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(post.title,style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.black)),
                      Text(post.date.toString(),style: TextStyle(fontWeight: FontWeight.normal, fontSize: 10, color: Colors.black)),
                    ],
                  ),
                )
            ),
            Container(
              height: 10,
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5,
        margin: EdgeInsets.all(10),
      ),
    );





    //   ListTile(
    //     leading: Text('${post.id}', style: textTheme.caption),
    //     title: Text(post.title),
    //     isThreeLine: true,
    //     subtitle: Text(post.body),
    //     dense: true,
    //   ),
    // );
  }
}