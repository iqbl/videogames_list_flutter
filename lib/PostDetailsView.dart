import 'package:videogames_list_flutter/posts/models/post.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PostDetailsView extends StatelessWidget {
  final Post post;

  PostDetailsView({required this.post});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(post.title),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            margin: new EdgeInsets.symmetric(horizontal: 18, vertical: 10),
            child: Column(
              children: [
                Stack(
                  children: [
                    Image.network(
                      "https://image.tmdb.org/t/p/w500"+post.image,
                      fit: BoxFit.fill,
                    ),
                  ],
                ),
                Container(
                  height: 20,
                ),
                Text(post.body)
              ],
            ),
          ),
        ),
      )
    );
  }
}