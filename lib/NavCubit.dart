import 'package:videogames_list_flutter/posts/models/post.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NavCubit extends Cubit<Post> {
  NavCubit() : super(new Post(id: 1, title: '', body: '', image: 'image', date: 'date'));

  void showPostDetails(Post post) => emit(post);

  void popToPosts() => emit(new Post(id: 1, title: '', body: '', image: 'image', date: 'date'));
}